"""Setup file for tomo-enki package."""

from setuptools import setup

setup(
    name='tomoenki',
    version='0.1',
    author='Andrew A. Herzing',
    description='Perform tomographic reconstruction using the ASTRA toolbox '
                'on the NIST ENKI HPC cluster',
    packages=['tomoenki']
)

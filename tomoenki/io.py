import os
import subprocess
import time


def send_to_enki(filename, username):
    """
    Send tomography dataset to ENKI.

    Args
    ====
    filename : str
        Name of the .hdf5 or .hspy file to parse

    username : str
        Username for ENKI.

    """
    if type(filename) is str:
        server_filename = os.path.split(filename)[1]
        upload_command = "scp %s %s@enki.nist.gov:/wrk/%s/tomodata/%s" %\
            (filename, username, username, server_filename)
        proc = subprocess.Popen(upload_command,
                                stdout=subprocess.PIPE,
                                shell=True)
        (out, err) = proc.communicate()

    elif type(filename) is list:
        for i in filename:
            server_filename = os.path.split(i)[1]
            print("Local file: %s" % i)
            print("Remote file: /wrk/%s/tomodata/%s" %
                  (username, server_filename))
            upload_command = "scp %s %s@enki.nist.gov:/wrk/%s/tomodata/%s" %\
                (i, username, username, server_filename)
            proc = subprocess.Popen(upload_command,
                                    stdout=subprocess.PIPE,
                                    shell=True)
            (out, err) = proc.communicate()
    return


def get_from_enki(datapath, username, extension='npy'):
    """
    Retrieve data from ENKI.

    Args
    ====
    datapath : str
        Path to data to be retrieved

    username : str
        Username for ENKI.

    extension : str
        Filename extension.  All files in datapath of this type will be
        retrieved.

    """
    recfiles = get_file_list(username, datapath, extension)
    print("Found %s %s files in data path" % (len(recfiles), extension))
    for i in recfiles:
        print("Remote file to transfer: %s" % i)
        filename = "./" + i.split('/')[-1]
        print("Local file: %s" % filename)
        downnload_command = "scp aherzing@enki.nist.gov:%s %s" % (i, filename)
        proc = subprocess.Popen(downnload_command,
                                stdout=subprocess.PIPE,
                                shell=True)
        (out, err) = proc.communicate()
    return filename


def get_file_list(username, datapath, extension):
    command = """ssh -m hmac-sha2-256 %s@enki.nist.gov ls %s/*.%s"""\
              % (username, datapath, extension)
    proc = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()
    files = out.split()
    files = [i.decode() for i in files]
    return files


def run_remote(username, datapath, extension, iterations=100, email=True):
    script = "~/code/tomo-enki/tomoenki/slurm/tomo_sirt3d.slurm"
    files = get_file_list(username, datapath, extension)
    """Execute reconstruction"""
    for i in files:
        print('Reconstructing %s' % i)
        if email:
            recon_command = "ssh -m hmac-sha2-256 %s@enki.nist.gov\
                'sbatch --mail-type=END --mail-user=%s@nist.gov %s %s %s'"\
                % (username, username, script, i, iterations)
            proc = subprocess.Popen(recon_command,
                                    stdout=subprocess.PIPE,
                                    shell=True)
            (out, err) = proc.communicate()
            print(out.decode())
        else:
            recon_command = "ssh -m hmac-sha2-256 %s@enki.nist.gov "\
                "'sbatch %s %s %s'"\
                % (username, script, i, iterations)
            proc = subprocess.Popen(recon_command,
                                    stdout=subprocess.PIPE,
                                    shell=True)
            (out, err) = proc.communicate()
            print(out.decode())
    return


def run_local(files, iterations=100, email=False, username=None):
    if email:
        if not username:
            username = os.getlogin()
        print("Email notfication(s) will be sent to %s@nist.gov" % username)

    script_file = "./tomo_script.slurm"
    with open(script_file, 'w') as h:
        h.write("#!/bin/bash\n")
        h.write("#SBATCH -J astra_tomography\n")
        h.write("#SBATCH -c 40\n")
        h.write("#SBATCH --gres=gpu:1\n")
        h.write("#SBATCH --partition=gpu\n")
        h.write("#SBATCH --time=01:00:00\n")

        h.write("\nsource /opt/anaconda3/etc/profile.d/conda.sh\n")
        h.write("conda activate tomo\n")
        h.write("python ~/code/tomo-enki/tomoenki/slurm/run_sirt3d.py $1 $2\n")
        h.write("echo Reconstruction completed!\n")

    start_time = time.time()
    rec_files = [None]*len(files)

    """Execute reconstruction"""
    idx = 0
    for i in files:
        print('Reconstructing %s of %s files' % (idx+1, len(files)))
        if email:
            recon_command = "sbatch --wait --mail-type=END "\
                "--mail-user=%s@nist.gov %s %s %s'"\
                % (username, script_file, i, iterations)
            proc = subprocess.Popen(recon_command,
                                    stdout=subprocess.PIPE,
                                    shell=True)
            (out, err) = proc.communicate()
        else:
            recon_command = "sbatch --wait %s %s %s" %\
                (script_file, i, iterations)
            proc = subprocess.Popen(recon_command,
                                    stdout=subprocess.PIPE,
                                    shell=True)
            (out, err) = proc.communicate()

        print("Reconstruction complete")
        print("--- %s seconds ---" % (time.time() - start_time))

        jobid = out.decode()[:-1].split()[-1]
        logfile = "slurm-" + jobid + ".out"
        with open(logfile, 'r') as f:
            lines = f.read()
        print(lines)
        rec_files[idx] = lines.split('\n')[2].split()[-1]

        idx += 1
    return rec_files

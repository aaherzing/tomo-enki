import astra
import numpy as np
import sys
import os
import h5py as h5


def get_h5_data(filename):
    """
    Function to extract tilts and image series data from a TomoTools hdf5 file.
    The function first looks to see if the Tomography entry is contained in
    the metadata library. If so, it pulls the tilts from there.  Otherwise,
    it attempts to extract the tilts from the Hyperspy axes information
    (this option may or may not be accurate and does not support non-linear
    tilts).

    Args
    ====
    filename : str
        Name of the .hdf5 or .hspy file to parse

    Returns
    =======
    data : NumPy array
        3D array containing the tilt series data. Dimensions are:
        [Theta, Y, X]

    tilts : NumPy array
        Array containing the tilt values for the dataset
    """

    def find_data(name):
        if '/data' in name:
            return name

    def find_tilts(name):
        if 'metadata/Tomography/tilts' in name:
            return name

    def find_axis_0(name):
        if 'axis-0' in name:
            return name

    if os.path.splitext(filename)[1].lower() in [".hdf5", ".hspy"]:
        h5file = h5.File(filename)
    tilt_item = h5file.visit(find_tilts)
    if tilt_item:
        tilts = h5file[tilt_item][...]
    if type(tilts) is np.ndarray:
        print('Found tilts in TomoTools metadata')
    else:
        axis_item = h5file.visit(find_axis_0)
        params = dict(h5file[axis_item].attrs)
        scale = params['scale']
        offset = params['offset']
        size = params['size']
        tilts = np.arange(offset, size + 1, scale)

    data_item = h5file.visit(find_data)
    data = h5file[data_item][...]
    h5file.close()
    return data, tilts


filename = sys.argv[1]
print("Input file: %s" % filename)
if len(sys.argv) > 2:
    iterations = np.int32(np.float(sys.argv[2]))
else:
    iterations = 100
print("Performing SIRT reconstruction with %s iterations" % iterations)
rootname, extension = os.path.splitext(filename)
outfilename = rootname + '_sirt%s.npy' % iterations
print("Output file: %s" % outfilename)

if extension == '.npy':
    stack = np.load(filename)
    angles = np.load(sys.argv[3])
else:
    stack, angles = get_h5_data(filename)

data = np.rollaxis(stack, 1)

if len(angles) != data.shape[1]:
    print("Truncating angles")
    angles = angles[:-1]

angles = np.pi * angles / 180

nchunks = np.int32(np.shape(data)[0] / 128)
if nchunks < 1:
    nchunks = 1
    chunksize = np.shape(data)[0]
else:
    chunksize = 128

pixelsX = data.shape[2]
pixelsY = data.shape[0]
pixelsZ = pixelsX

rec = np.zeros([pixelsY, pixelsZ, pixelsX], data.dtype)
for i in range(0, nchunks):
    print("Reconstructing chunk number %s of %s" % (i + 1, nchunks))
    chunk = data[i * chunksize:(i + 1) * chunksize, :, :]
    vol_geom = astra.create_vol_geom(pixelsZ, pixelsX, chunksize)
    proj_geom = astra.create_proj_geom('parallel3d',
                                       1.0, 1.0,
                                       chunksize, pixelsX, angles)

    rec_id = astra.data3d.create('-vol', vol_geom)
    sinogram_id = astra.data3d.create('-proj3d', proj_geom, chunk)

    cfg = astra.astra_dict('SIRT3D_CUDA')
    cfg['ReconstructionDataId'] = rec_id
    cfg['ProjectionDataId'] = sinogram_id

    cfg['option'] = {}
    cfg['option']['MinConstraint'] = 0
    alg_id = astra.algorithm.create(cfg)

    # astra.data3d.store(sinogram_id, data)
    astra.algorithm.run(alg_id, iterations)
    rec[i * chunksize:(i + 1) * chunksize, :, :] = astra.data3d.get(rec_id)

    astra.algorithm.delete(alg_id)
    astra.data3d.delete(sinogram_id)
    astra.data3d.delete(rec_id)

np.save(outfilename, np.uint16(rec), allow_pickle=False)

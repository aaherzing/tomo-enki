from copy import deepcopy
import numpy as np
import cv2
from scipy import optimize
from tomoenki import recon


def trans_stack(stack, xshift=0.0, yshift=0.0, angle=0.0):
    transformed = deepcopy(stack)
    theta = np.pi * angle / 180.
    center_y, center_x = np.float32(np.array(transformed.shape[1:])/2)

    rot_mat = np.array([[np.cos(theta), -np.sin(theta), 0],
                        [np.sin(theta), np.cos(theta), 0],
                        [0, 0, 1]])

    trans_mat = np.array([[1, 0, center_x],
                          [0, 1, center_y],
                          [0, 0, 1]])

    rev_mat = np.array([[1, 0, -center_x],
                        [0, 1, -center_y],
                        [0, 0, 1]])

    rotation_mat = np.dot(np.dot(trans_mat, rot_mat), rev_mat)

    shift = np.array([[1, 0, np.float32(xshift)],
                      [0, 1, np.float32(-yshift)],
                      [0, 0, 1]])

    full_transform = np.dot(shift, rotation_mat)

    mode = cv2.INTER_LINEAR

    for i in range(0, stack.shape[0]):
        transformed[i, :, :] = \
            cv2.warpAffine(transformed[i, :, :],
                           full_transform[:2, :],
                           transformed.shape[1:][::-1],
                           flags=mode)
    return transformed


def tilt_minimize(stack, angles, boundaries=None, tol=0.5, cuda=False):
    def align_stack_cuda(x, stack, angles, slices=None):
        xshift = x[0]
        angle = x[1]
        if slices is None:
            middle = np.int32(stack.shape[2]/2)
            slices = [middle-10, middle+10]
        trans = trans_stack(stack, xshift=xshift, angle=angle)
        sino = np.zeros([stack.shape[0],
                         len(slices),
                         stack.shape[2]])
        for i in range(0, len(slices)):
            sino = deepcopy(trans[:, slices[i], :])
        rec = recon.astra_sirt(sino, angles, 50, cuda=True)
        proj = recon.astra_project(rec, angles, cuda=True)
        diff = np.abs(proj-sino)
        error = diff.sum()
        return error

    ali = deepcopy(stack)
    if boundaries is None:
        boundaries = ((-30, 30), (-5, 5),)

    result = optimize.differential_evolution(align_stack_cuda,
                                             bounds=boundaries,
                                             args=(ali, angles),
                                             disp=False,
                                             callback=None,
                                             tol=0.5)

    shift, rotation = result['x']
    print('Shift: %.2f, Rotation: %.2f' % (shift, rotation))

    out = trans_stack(ali, xshift=shift, angle=rotation)
    return out

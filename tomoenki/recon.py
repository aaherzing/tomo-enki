import numpy as np
import astra
import os
from tomoenki import io


class AstraToolbox:

    def __init__(self, data, angles=None, thickness=None):
        '''
        Initialize the ASTRA toolbox with a simple parallel configuration.
        The image is assumed to be square, and the detector count is equal
        to the number of rows/columns.
        '''

        if type(angles) is np.ndarray:
            angles = np.pi * angles / 180
        else:
            angles = np.linspace(0, np.pi, data.shape[0], False)

        if len(data.shape) == 2:
            self.data = np.expand_dims(data, 1)
        else:
            self.data = data

        pixelsX = self.data.shape[2]
        pixelsY = self.data.shape[1]
        if not thickness:
            pixelsZ = pixelsX
        else:
            pixelsZ = thickness

        self.vol_geom = astra.create_vol_geom(pixelsX, pixelsZ)
        self.proj_geom = astra.create_proj_geom('parallel', 1.0, pixelsX,
                                                angles)
        self.proj_id = astra.create_projector('strip', self.proj_geom,
                                              self.vol_geom)

        self.rec_id = astra.data2d.create('-vol', self.vol_geom)
        self.sinogram_id = astra.data2d.create('-sino', self.proj_geom,
                                               self.data[:, 0, :])
        self.rec = np.zeros([pixelsY, pixelsZ, pixelsX])

    def backproj(self, filter=False, filter_type='ram-lak'):
        cfg = astra.astra_dict('FBP')
        cfg['ReconstructionDataId'] = self.rec_id
        cfg['ProjectionDataId'] = self.sinogram_id
        cfg['ProjectorId'] = self.proj_id
        cfg['option'] = {}
        if filter:
            cfg['option']['FilterType'] = filter_type
        else:
            cfg['option']['FilterType'] = 'none'
        alg_id = astra.algorithm.create(cfg)

        for i in range(0, self.data.shape[1]):
            astra.data2d.store(self.sinogram_id, self.data[:, i, :])
            astra.algorithm.run(alg_id)
            self.rec[i, :, :] = astra.data2d.get(self.rec_id).T
        astra.algorithm.delete(alg_id)
        astra.data2d.delete(self.proj_id)
        astra.data2d.delete(self.rec_id)

    def sirt(self, iterations=25, constrain=True):
        cfg = astra.astra_dict('SIRT')
        cfg['ReconstructionDataId'] = self.rec_id
        cfg['ProjectionDataId'] = self.sinogram_id
        cfg['ProjectorId'] = self.proj_id
        if constrain:
            cfg['option'] = {}
            cfg['option']['MinConstraint'] = 0
        alg_id = astra.algorithm.create(cfg)

        for i in range(0, self.data.shape[1]):
            astra.data2d.store(self.sinogram_id, self.data[:, i, :])
            astra.algorithm.run(alg_id, iterations)
            self.rec[i, :, :] = astra.data2d.get(self.rec_id).T
        astra.algorithm.delete(alg_id)
        astra.data2d.delete(self.proj_id)
        astra.data2d.delete(self.rec_id)

    def proj(self, slice_data):
        sid, proj_data = astra.create_sino(slice_data, self.proj_id)
        astra.data2d.delete(sid)
        return proj_data


def run(h5filename, method='FBP', iterations=25, constrain=True,
        thickness=None, nslices=None, verbose=True, log=True):
    stack, tilts = io.get_data(h5filename)
    outfile = os.path.splitext(h5filename)[0] + '_rec'

    if not nslices:
        sino = stack
    else:
        sino = stack[:, nslices, :]
    if not thickness:
        thickness = stack.shape[2]
    AST = AstraToolbox(sino, angles=tilts, thickness=thickness)

    if method.upper() == 'BP':
        AST.backproj(False)
    elif method.upper() == 'FBP':
        AST.backproj(True)
    elif method.upper() == 'SIRT':
        AST.sirt(iterations, constrain)
    else:
        raise ValueError('Unknown reconstruction method %s' % method)

    if verbose:
        print('\nASTRA Reconstruction')
        print('*******************************\n')
        print('Tilt series file: %s' % h5filename)
        print('Reconstruction output file: %s' % outfile)
        print('Reconstruction algorithm: %s' % method.upper())

    if log:
        logfile = os.path.splitext(h5filename)[0] + '_rec.log'
        with open(logfile, 'w') as h:
            h.write('ASTRA Reconstruction\n')
            h.write('*******************************\n\n')
            h.write('Tilt series file: %s\n' % h5filename)
            h.write('Reconstruction output file: %s\n' % outfile)
            h.write('Reconstruction algorithm: %s' % method.upper())

    return AST.rec
